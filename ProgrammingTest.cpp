// ProgrammingTest

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <set>

using namespace std;

void findLongestPath(const string& name,
    const map<string, vector<string>>& tree,
    vector<string>& currentPath,
    vector<string>& longestPath) {
    currentPath.push_back(name);

    if (tree.find(name) == tree.end()) {
        if (currentPath.size() > longestPath.size()) {
            longestPath = currentPath;
        }
    }
    else {
        for (const auto& parent : tree.at(name)) {
            findLongestPath(parent, tree, currentPath, longestPath);
        }
    }

    currentPath.pop_back();
}

string findOldestParent(const string& name, const vector<pair<string, string>>& relationships) {
    map<string, vector<string>> tree;
    set<string> parents;

    for (const auto& relationship : relationships) {
        const string& child = relationship.first;
        const string& parent = relationship.second;

        if (!parent.empty()) {
            tree[child].push_back(parent);
            parents.insert(parent);
        }
        parents.insert(child);
    }

    if (parents.find(name) == parents.end() && !tree.count(name)) {
        return name;
    }

    vector<string> currentPath;
    vector<string> longestPath;

    findLongestPath(name, tree, currentPath, longestPath);

    if (longestPath.empty()) {
        return name;
    }

    return longestPath.back();
}

int main() {
    vector<pair<string, string>> relationships = {
        {"Tom", ""},
        {"Mary", "Tom"},
        {"Jack", "Tom"},
        {"Anna", "Mary"},
        {"Greg", "Anna"},
        {"Greg", "Jack"}
    };

    string name = "Greg";

    string oldestParent = findOldestParent(name, relationships);

    if (!oldestParent.empty()) {
        cout << "The oldest parent of " << name << " is " << oldestParent << endl;
    }
    else {
        cout << name << " has no ancestors." << endl;
    }

    return 0;
}
